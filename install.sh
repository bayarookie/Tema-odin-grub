#!/bin/bash

GRUB_NAME=""
THEME_DIR="tema-odin"

function update_grub_file() {
  TMP1=$(date '+%Y-%m-%d_%H:%M:%S')
  echo -e "\e[1m\e[32m==>\e[97m Copying /etc/default/grub to /tmp/grub_${TMP1}"
  cp /etc/default/grub /tmp/grub_${TMP1}
  grep -E -v 'GRUB_THEME|GRUB_BACKGROUND' < /etc/default/grub > /tmp/clean_grub
  mv /tmp/clean_grub /etc/default/grub
  echo "GRUB_THEME=/boot/${GRUB_NAME}/themes/${THEME_DIR}/theme.txt" >> /etc/default/grub
  TERM1=/boot/${GRUB_NAME}/themes/${THEME_DIR}/terminal_c.png
  echo -e "\e[1m\e[32m==>\e[97m If file exists: ${TERM1} ???"
  if [ -f ${TERM1} ]; then
    echo -e "\e[1m\e[32m==>\e[97m Adding string to /etc/default/grub"
    echo "GRUB_BACKGROUND=${TERM1}" >> /etc/default/grub
  fi
}

function compile_grub() {
  echo -e "\e[1m\e[32m==>\e[97m Applying changes from /boot/${GRUB_NAME}/grub.cfg ...\e[0m"
  ${GRUB_NAME}-mkconfig -o /boot/${GRUB_NAME}/grub.cfg
  echo -e "\e[1m\e[34m  ->\e[97m Theme successfuly applied!"
  echo -e "\e[1m\e[34m  ->\e[97m Restart your PC to check it out."
  sleep 2
}

# Check user is root
if [ $UID == 0 ]; then
  echo "Yes, You are root!"
else
  echo "No, You must be root!"
  exit 1
fi

# Check which grub
if [ -d "/boot/grub" ]; then
  GRUB_NAME="grub"
else
  GRUB_NAME="grub2"
fi

# Copy files
echo -e "\e[1m\e[32m==>\e[97m Copying files to /boot/${GRUB_NAME}/themes/ ...\e[0m"
cp -rf ${THEME_DIR} /boot/${GRUB_NAME}/themes/

echo -e "\e[1m\e[97m  You must set the theme in your GRUB config file,"
while : ;do
  if [ "$answer" = "g" ];then
    echo -e "\e[1m\e[97m  You didn't give a valid option, try again."
  else
    read -p "  Would you like to do it now? [y/n] " -t 10 answer
    echo -e "\e[0m"
    if [ "$answer" = "y" ];then
      update_grub_file
      compile_grub
      break
    elif [ "$answer" = "n" ];then
      break
    fi
    let answer=g
  fi
done

exit 0
