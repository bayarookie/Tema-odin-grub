#!/bin/sh

convert tema-odin/background.png \
-gravity Center -crop 1920x1080+30+260 \
-gravity southeast -splice 30x260 tema-odin/terminal_c.png
